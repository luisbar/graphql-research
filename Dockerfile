FROM node:14.0.0

WORKDIR /app

RUN npm install -g lerna

COPY package.json .
COPY package-lock.json .
COPY lerna.json .
COPY packages/shared packages/shared/

RUN lerna bootstrap
