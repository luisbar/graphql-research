## What is this?

Monorepo which contains the following packages:

- graphql-express: this contains an example about [express](https://expressjs.com/) and [graphql](https://graphql.org/), which will run on the port `4000`
- graphql-yoga-typeorm: this contains an example about [graphql-yoga](https://github.com/prisma-labs/graphql-yoga) and [typeorm](https://typeorm.io/#/), which will run on the port `4001`
- graphql-yoga-prisma: this contains an example about [graphql-yoga](https://github.com/prisma-labs/graphql-yoga) and [prisma](https://typeorm.io/#/), which will run on the port `4002`, additionally on the port `5555` will run a dashboard provided by prisma which permit you visualize, update and add new record to your database

:information_source: graphql-yoga comes with [Graphql Playgorund](https://github.com/graphql/graphql-playground) out of the box, so, you can use it introducing `localhost:4001` or `localhost:4002`
on your browser

## How to run?

1. Install nvm

    Instructions [here](https://github.com/nvm-sh/nvm#installing-and-updating)
2. Install and use a node version
    ```bash
    nvm install && nvm use
    ```

3. Install lerna
    ```bash
    npm i -g lerna
    ```

4. Install dependencies of all packages using lerna
    ```bash
    lerna bootstrap
    ```
5. Build docker images
    ```bash
    npm run build
    ```
6. Run containers

    ```bash
    npm start
    ```

## Examples of request received and responses returned by the three packages

- get teachers
    ```graphql
    {
        teachers {
            id
            fullName
            email
            subjects {
                id
                name
            }
        }
    }
    ```
    ```json
    {
        "data": {
            "teachers": [
                {
                    "id": "1",
                    "fullName": "Elva Asano",
                    "email": "elva@cba.com",
                    "subjects": [
                        {
                            "id": "1",
                            "name": "Mobile design"
                        },
                        {
                            "id": "2",
                            "name": "Web design"
                        }
                    ]
                },
                {
                    "id": "2",
                    "fullName": "Juan Serpentegui",
                    "email": "juan@cba.com",
                    "subjects": [
                        {
                            "id": "3",
                            "name": "Mobile dev"
                        }
                    ]
                },
                {
                    "id": "3",
                    "fullName": "Daniel Muguertegui",
                    "email": "dani@cba.com",
                    "subjects": [
                        {
                            "id": "4",
                            "name": "Web dev"
                        }
                    ]
                }
            ]
        }
    }
    ```
- get students
    ```graphql
    {
        students {
            id
            fullName
            email
            subscriptions {
                id
                school {
                    id
                    name
                }
            }
        }
    }
    ```
    ```json
    {
        "data": {
            "students": [
                {
                    "id": "1",
                    "fullName": "Luis Angel Barrancos Ortiz",
                    "email": "luis@minka.io",
                    "subscriptions": [
                        {
                            "id": "1",
                            "school": {
                                "id": "1",
                                "name": "Mobile development"
                            }
                        },
                        {
                            "id": "2",
                            "school": {
                                "id": "2",
                                "name": "Web development"
                            }
                        }
                    ]
                },
                {
                    "id": "2",
                    "fullName": "Juan Sebastian Barrancos Ortiz",
                    "email": "sebas@minka.io",
                    "subscriptions": [
                        {
                            "id": "3",
                            "school": {
                                "id": "2",
                                "name": "Web development"
                            }
                        }
                    ]
                },
                {
                    "id": "3",
                    "fullName": "Natalia Barrancos Ortiz",
                    "email": "nati@minka.io",
                    "subscriptions": []
                }
            ]
        }
    }
    ```
- get tasks
    ```graphql
    {
        tasks {
            id
            name
            subject {
                id
                name
            }
        }
    }
    ```
    ```json
    {
        "data": {
            "tasks": [
                {
                    "id": "1",
                    "name": "task 1",
                    "subject": {
                        "id": "1",
                        "name": "Mobile design"
                    }
                },
                {
                    "id": "2",
                    "name": "task 2",
                    "subject": {
                        "id": "1",
                        "name": "Mobile design"
                    }
                },
                {
                    "id": "3",
                    "name": "task 1",
                    "subject": {
                        "id": "2",
                        "name": "Web design"
                    }
                },
                {
                    "id": "4",
                    "name": "task 2",
                    "subject": {
                        "id": "2",
                        "name": "Web design"
                    }
                },
                {
                    "id": "5",
                    "name": "task 1",
                    "subject": {
                        "id": "3",
                        "name": "Mobile dev"
                    }
                },
                {
                    "id": "6",
                    "name": "task 1",
                    "subject": {
                        "id": "4",
                        "name": "Web dev"
                    }
                }
            ]
        }
    }
    ```
- get subjects

    ```graphql
    {
        subjects {
            id
            name
            tasks {
                id
                name
            }
            teacher {
                id
                fullName
            }
            school {
                id
                name
            }
        }
    }
    ```
    ```json
    {
        "data": {
            "subjects": [
                {
                    "id": "1",
                    "name": "Mobile design",
                    "tasks": [
                        {
                            "id": "1",
                            "name": "task 1"
                        },
                        {
                            "id": "2",
                            "name": "task 2"
                        }
                    ],
                    "teacher": {
                        "id": "1",
                        "fullName": "Elva Asano"
                    },
                    "school": {
                        "id": "2",
                        "name": "Web development"
                    }
                },
                {
                    "id": "2",
                    "name": "Web design",
                    "tasks": [
                        {
                            "id": "3",
                            "name": "task 1"
                        },
                        {
                            "id": "4",
                            "name": "task 2"
                        }
                    ],
                    "teacher": {
                        "id": "1",
                        "fullName": "Elva Asano"
                    },
                    "school": {
                        "id": "2",
                        "name": "Web development"
                    }
                },
                {
                    "id": "3",
                    "name": "Mobile dev",
                    "tasks": [
                        {
                            "id": "5",
                            "name": "task 1"
                        }
                    ],
                    "teacher": {
                        "id": "2",
                        "fullName": "Juan Serpentegui"
                    },
                    "school": {
                        "id": "1",
                        "name": "Mobile development"
                    }
                },
                {
                    "id": "4",
                    "name": "Web dev",
                    "tasks": [
                        {
                            "id": "6",
                            "name": "task 1"
                        }
                    ],
                    "teacher": {
                        "id": "3",
                        "fullName": "Daniel Muguertegui"
                    },
                    "school": {
                        "id": "2",
                        "name": "Web development"
                    }
                }
            ]
        }
    }
    ```
- get schools
    ```graphql
    {
        schools {
            id
            name
            subjects {
                id
                name
            }
            subscriptions {
                id
            }
        }
    }
    ```
    ```json
    {
        "data": {
            "schools": [
                {
                    "id": "1",
                    "name": "Mobile development",
                    "subjects": [
                        {
                            "id": "1",
                            "name": "Mobile design"
                        },
                        {
                            "id": "2",
                            "name": "Web design"
                        }
                    ],
                    "subscriptions": [
                        {
                            "id": "1"
                        }
                    ]
                },
                {
                    "id": "2",
                    "name": "Web development",
                    "subjects": [
                        {
                            "id": "3",
                            "name": "Mobile dev"
                        },
                        {
                            "id": "4",
                            "name": "Web dev"
                        }
                    ],
                    "subscriptions": [
                        {
                            "id": "2"
                        },
                        {
                            "id": "3"
                        }
                    ]
                }
            ]
        }
    }
    ```
- get subscriptions
    ```graphql
    {
        subscriptions {
            id
            student {
                id
                fullName
            }
            school {
                id
                name
            }
        }
    }
    ```
    ```json
    {
        "data": {
            "subscriptions": [
                {
                    "id": "1",
                    "student": {
                        "id": "1",
                        "fullName": "Luis Angel Barrancos Ortiz"
                    },
                    "school": {
                        "id": "1",
                        "name": "Mobile development"
                    }
                },
                {
                    "id": "2",
                    "student": {
                        "id": "1",
                        "fullName": "Luis Angel Barrancos Ortiz"
                    },
                    "school": {
                        "id": "2",
                        "name": "Web development"
                    }
                },
                {
                    "id": "3",
                    "student": {
                        "id": "2",
                        "fullName": "Juan Sebastian Barrancos Ortiz"
                    },
                    "school": {
                        "id": "2",
                        "name": "Web development"
                    }
                }
            ]
        }
    }
    ```
- get one teacher
    ```graphql
    {
        teacher(id: 1) {
            id
            fullName
            email
            subjects {
                id
                name
            }
        }
    }
    ```
    ```json
    {
        "data": {
            "teacher": {
                "id": "1",
                "fullName": "Elva Asano",
                "email": "elva@cba.com",
                "subjects": [
                    {
                        "id": "1",
                        "name": "Mobile design"
                    },
                    {
                        "id": "2",
                        "name": "Web design"
                    }
                ]
            }
        }
    }
    ```

