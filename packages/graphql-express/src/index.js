const express = require('express')
const { graphql, buildSchema } = require('graphql');
const app = express()
const logger = console.log
const { datasources, schema } = require('shared')

// Resolvers
// obj: the previous object, which for a field on the root Query type is often not used
// args: the arguments provided to the field in the GraphQL query
// context: a value which is provided to every resolver and holds important contextual information like the currently logged in user, or access to a database
// info: a value which holds field-specific information relevant to the current query as well as the schema details
const resolvers = {
  teachers: () => datasources.memory.teachers.map((teacher) => ({
    ...teacher,
    subjects: datasources.memory.subjects.filter((subject) => subject.teacher.id === teacher.id)
  })),
  students: () => datasources.memory.students.map((student) => ({
    ...student,
    subscriptions: datasources.memory.subscriptions.filter((subscription) => subscription.student.id === student.id)
  })),
  tasks: () => datasources.memory.tasks.map((task) => ({
    ...task,
    subject: datasources.memory.subjects.filter((subject) => subject.tasks.findIndex((taskAssigned) => taskAssigned.id === task.id)).pop()
  })),
  subjects: () => datasources.memory.subjects.map((subject) => ({
    ...subject,
    school: datasources.memory.schools.filter((school) => school.subjects.findIndex((subjectAssigned) => subjectAssigned.id === subject.id)).pop()
  })),
  schools: () => datasources.memory.schools.map((school) => ({
    ...school,
    subscriptions: datasources.memory.subscriptions.filter((subscription) => subscription.school.id === school.id)
  })),
  subscriptions: () => datasources.memory.subscriptions,
  teacher: (args, obj, context, info) => {
    const { id } = args
    return datasources.memory.teachers.map((teacher) => ({
      ...teacher,
      subjects: datasources.memory.subjects.filter((subject) => subject.teacher.id === teacher.id)
    })).find((teacher) => teacher.id === id)
  },
  student: (args, obj, context, info) => {
    const { id } = args
    return datasources.memory.students.map((student) => ({
      ...student,
      subscriptions: datasources.memory.subscriptions.filter((subscription) => subscription.student.id === student.id)
    })).find((student) => student.id === id)
  },
  task: (args, obj, context, info) => {
    const { id } = args
    return datasources.memory.tasks.map((task) => ({
      ...task,
      subject: datasources.memory.subjects.filter((subject) => subject.tasks.findIndex((taskAssigned) => taskAssigned.id === task.id)).pop()
    })).find((task) => task.id === id)
  },
  subject: (args, obj, context, info) => {
    const { id } = args
    return datasources.memory.subjects.map((subject) => ({
      ...subject,
      school: datasources.memory.schools.filter((school) => school.subjects.findIndex((subjectAssigned) => subjectAssigned.id === subject.id)).pop()
    })).find((subject) => subject.id === id)
  },
  school: (args, obj, context, info) => {
    const { id } = args
    return datasources.memory.schools.map((school) => ({
      ...school,
      subscriptions: datasources.memory.subscriptions.filter((subscription) => subscription.school.id === school.id)
    })).find((school) => school.id === id)
  },
  subscription: (args, obj, context, info) => {
    const { id } = args
    return datasources.memory.subscriptions.find((subscription) => subscription.id === id)
  },
}

// For supporting graphql query format
app.use(express.json())
// Just exposing a single endpoint
app.post('/', async (req, res) => {
  const result = await graphql(buildSchema(schema.join('')), req.body.query, resolvers)
  res.send(result)
})
// Start the server
app.listen(process.env.PORT, () => {
  logger(`Graphql and Express are runnning on port ${process.env.PORT}`)
})