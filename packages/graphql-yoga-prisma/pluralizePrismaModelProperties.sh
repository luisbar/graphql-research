#!/bin/bash
awk 'BEGIN{i=1} \
(index($2, "[]") != 0 && substr($1, length($1)) != "s"){$1=$1 "s"} \
{rec[i++]=$0} \
END{for(i=1;i<=NR;i++)print rec[i]}' ../shared/src/datasources/prisma/schema.prisma >> ../shared/src/datasources/prisma/schema.prisma.temp && mv ../shared/src/datasources/prisma/schema.prisma.temp ../shared/src/datasources/prisma/schema.prisma