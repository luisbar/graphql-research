const { GraphQLServer } = require('graphql-yoga')
const logger = console.log
const { datasources, schema } = require('shared')

// Resolvers
// obj: the previous object, which for a field on the root Query type is often not used
// args: the arguments provided to the field in the GraphQL query
// context: a value which is provided to every resolver and holds important contextual information like the currently logged in user, or access to a database
// info: a value which holds field-specific information relevant to the current query as well as the schema details
const resolvers = {
  Query: {
    teachers: async () => {
      const connection = datasources.prisma()
      return await connection
      .teacher
      .findMany({
        include: {
          subjects: true
        }
      })
    },
    students: async () => {
      const connection = datasources.prisma()
      return await connection
      .student
      .findMany({
        include: {
          subscriptions: {
            include: {
              school: true,
            },
          },
        },
      })
    },
    tasks: async () => {
      const connection = datasources.prisma()
      return await connection
      .task
      .findMany({
        include: {
          subject: {
            include: {
              school: true,
              teacher: true,
            },
          },
        },
      })
    },
    subjects: async () => {
      const connection = datasources.prisma()
      return await connection
      .subject
      .findMany({
        include: {
          tasks: true,
          teacher: true,
          school: true,
        },
      })
    },
    schools: async () => {
      const connection = datasources.prisma()
      return await connection
      .school
      .findMany({
        include: {
          subjects: true,
          subscriptions: true,
        },
      })
    },
    subscriptions: async () => {
      const connection = datasources.prisma()
      return await connection
      .subscription
      .findMany({
        include: {
          student: true,
          school: true,
        },
      })
    },
    teacher: async (obj, args, context, info) => {
      const { id } = args
      const connection = datasources.prisma()
      return await connection
      .teacher
      .findOne({
        where: {
          id,
        },
        include: {
          subjects: true
        }
      })
    },
    student: async (obj, args, context, info) => {
      const { id } = args
      const connection = datasources.prisma()
      return await connection
      .student
      .findOne({
        where: {
          id,
        },
        include: {
          subscriptions: {
            include: {
              school: true,
            },
          },
        },
      })
    },
    task: async (obj, args, context, info) => {
      const { id } = args
      const connection = datasources.prisma()
      return await connection
      .task
      .findOne({
        where: {
          id,
        },
        include: {
          subject: {
            include: {
              school: true,
              teacher: true,
            },
          },
        },
      })
    },
    subject: async (obj, args, context, info) => {
      const { id } = args
      const connection = datasources.prisma()
      return await connection
      .subject
      .findOne({
        where: {
          id,
        },
        include: {
          tasks: true,
          teacher: true,
          school: true,
        },
      })
    },
    school: async (obj, args, context, info) => {
      const { id } = args
      const connection = await datasources.typeorm
      return await connection
      .getRepository('school')
      .findOne({
        relations: ['subjects', 'subscriptions'],
        where: { id },
      })
    },
    subscription: async (obj, args, context, info) => {
      const { id } = args
      const connection = datasources.prisma()
      return await connection
      .subscription
      .findOne({
        where: {
          id,
        },
        include: {
          student: true,
          school: true,
        },
      })
    },
  }
}

// Create an instance of GraphQLServer
const server = new GraphQLServer({ typeDefs: schema.join(''), resolvers })
// Start the server
server.start({ port: process.env.PORT },() => logger(`Graphql Yoga and Prisma is runnning on port ${process.env.PORT}`))

