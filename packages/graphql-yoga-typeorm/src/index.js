const { GraphQLServer } = require('graphql-yoga')
const logger = console.log
const { datasources, schema } = require('shared')

// Resolvers
// obj: the previous object, which for a field on the root Query type is often not used
// args: the arguments provided to the field in the GraphQL query
// context: a value which is provided to every resolver and holds important contextual information like the currently logged in user, or access to a database
// info: a value which holds field-specific information relevant to the current query as well as the schema details
const resolvers = {
  Query: {
    teachers: async () => {
      const connection = await datasources.typeorm
      return await connection
      .getRepository('teacher')
      .find({ relations: ['subjects', 'subjects.tasks', 'subjects.teacher', 'subjects.school'] })
    },
    students: async () => {
      const connection = await datasources.typeorm
      return await connection
      .getRepository('student')
      .find({ relations: ['subscriptions', 'subscriptions.school'] })
    },
    tasks: async () => {
      const connection = await datasources.typeorm
      return await connection
      .getRepository('task')
      .find({ relations: ['subject', 'subject.teacher', 'subject.school'] })
    },
    subjects: async () => {
      const connection = await datasources.typeorm
      return await connection
      .getRepository('subject')
      .find({ relations: ['tasks', 'teacher', 'school'] })
    },
    schools: async () => {
      const connection = await datasources.typeorm
      return await connection
      .getRepository('school')
      .find({ relations: ['subjects', 'subscriptions'] })
    },
    subscriptions: async () => {
      const connection = await datasources.typeorm
      return await connection
      .getRepository('subscription')
      .find({ relations: ['student', 'school'] })
    },
    teacher: async (obj, args, context, info) => {
      const { id } = args
      const connection = await datasources.typeorm
      return await connection
      .getRepository('teacher')
      .findOne({
        relations: ['subjects', 'subjects.tasks', 'subjects.teacher', 'subjects.school'],
        where: { id },
      })
    },
    student: async (obj, args, context, info) => {
      const { id } = args
      const connection = await datasources.typeorm
      return await connection
      .getRepository('student')
      .findOne({
        relations: ['subscriptions', 'subscriptions.school'],
        where: { id },
      })
    },
    task: async (obj, args, context, info) => {
      const { id } = args
      const connection = await datasources.typeorm
      return await connection
      .getRepository('task')
      .findOne({
        relations: ['subject', 'subject.teacher', 'subject.school'],
        where: { id },
      })
    },
    subject: async (obj, args, context, info) => {
      const { id } = args
      const connection = await datasources.typeorm
      return await connection
      .getRepository('subject')
      .findOne({
        relations: ['tasks', 'teacher', 'school'],
        where: { id },
      })
    },
    school: async (obj, args, context, info) => {
      const { id } = args
      const connection = await datasources.typeorm
      return await connection
      .getRepository('school')
      .findOne({
        relations: ['subjects', 'subscriptions'],
        where: { id },
      })
    },
    subscription: async (obj, args, context, info) => {
      const { id } = args
      const connection = await datasources.typeorm
      return await connection
      .getRepository('subscription')
      .findOne({
        relations: ['student', 'school'],
        where: { id },
      })
    },
  }
}

// Create an instance of GraphQLServer
const server = new GraphQLServer({ typeDefs: schema.join(''), resolvers })
// Start the server
server.start({ port: process.env.PORT },() => logger(`Graphql Yoga and Typeorm is runnning on port ${process.env.PORT}`))

