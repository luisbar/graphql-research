import memory from './memory'
import typeorm from './typeorm'
import prisma from './prisma'

export const datasources = {
  memory,
  typeorm,
  prisma,
}