const students = [
  {
    id: '1',
    fullName: 'Luis Angel Barrancos Ortiz',
    email: 'luis@minka.io',
    age: 29, 
  },
  {
    id: '2',
    fullName: 'Juan Sebastian Barrancos Ortiz',
    email: 'sebas@minka.io',
    age: 29, 
  },
  {
    id: '3',
    fullName: 'Natalia Barrancos Ortiz',
    email: 'nati@minka.io',
    age: 29, 
  },
]

const teachers = [
  {
    id: '1',
    fullName: 'Elva Asano',
    email: 'elva@cba.com',
    age: 29, 
  },
  {
    id: '2',
    fullName: 'Juan Serpentegui',
    email: 'juan@cba.com',
    age: 29, 
  },
  {
    id: '3',
    fullName: 'Daniel Muguertegui',
    email: 'dani@cba.com',
    age: 29, 
  },
]

const tasks = [
  {
    id: '1',
    name: 'task 1',
    description: 'Material design for mobile',
    dueDate: '10-01-2019',
  },
  {
    id: '2',
    name: 'task 2',
    description: 'Figma for mobile',
    dueDate: '10-02-2019',
  },
  {
    id: '3',
    name: 'task 1',
    description: 'Material design for web',
    dueDate: '10-03-2019',
  },
  {
    id: '4',
    name: 'task 2',
    description: 'Figma for web',
    dueDate: '10-04-2019',
  },
  {
    id: '5',
    name: 'task 1',
    description: 'React native',
    dueDate: '10-05-2019',
  },
  {
    id: '6',
    name: 'task 1',
    description: 'React',
    dueDate: '10-06-2019',
  },
]

const subjects = [
  {
    id: '1',
    name: 'Mobile design',
    tasks: [
      tasks[0], tasks[1],
    ],
    teacher: teachers[0],
  },
  {
    id: '2',
    name: 'Web design',
    tasks: [
      tasks[2], tasks[3],
    ],
    teacher: teachers[0],
  },
  {
    id: '3',
    name: 'Mobile dev',
    tasks: [
      tasks[4],
    ],
    teacher: teachers[1],
  },
  {
    id: '4',
    name: 'Web dev',
    tasks: [
      tasks[5],
    ],
    teacher: teachers[2],
  },
]

const schools = [
  {
    id: '1',
    name: 'Mobile development',
    subjects: [
      subjects[0],
      subjects[1],
    ],
  },
  {
    id: '2',
    name: 'Web development',
    subjects: [
      subjects[2],
      subjects[3],
    ],
  },
]

const subscriptions = [
  {
    id: '1',
    student: students[0],
    school: schools[0],
  },
  {
    id: '2',
    student: students[0],
    school: schools[1],
  },
  {
    id: '3',
    student: students[1],
    school: schools[1],
  },
]

export default {
  students,
  teachers,
  tasks,
  subjects,
  schools,
  subscriptions,
}