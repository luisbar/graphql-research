const { PrismaClient } = require('@prisma/client')
export default () => {
  return new PrismaClient()
}
