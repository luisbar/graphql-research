import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm'
import { Subject } from './subject'
import { Subscription } from './subscription'

@Entity({ name: 'school' })
export class School {

  @PrimaryColumn({ type: 'varchar', length: 50 })
  id: string

  @Column({ type: 'varchar', length: 200 })
  name: string

  @OneToMany(() => Subject, subject => subject.school)
  subjects: Subject[]

  @OneToMany(() => Subscription, subscription => subscription.school)
  subscriptions: Subscription[]

}