import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm'
import { Subscription } from './subscription'

@Entity({ name: 'student' })
export class Student {

  @PrimaryColumn({ type: 'varchar', length: 50 })
  id: string

  @Column({ type: 'varchar', length: 200 })
  fullName: string

  @Column({ type: 'varchar', length: 200 })
  email: string

  @Column({ type: 'int' })
  age: number

  @OneToMany(() => Subscription, subscription => subscription.student)
  subscriptions: Subscription[]

}