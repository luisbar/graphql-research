import { Entity, PrimaryColumn, Column, OneToMany, ManyToOne } from 'typeorm'
import { Task } from './task'
import { Teacher } from './teacher'
import { School } from './school'

@Entity({ name: 'subject' })
export class Subject {

  @PrimaryColumn({ type: 'varchar', length: 50 })
  id: string

  @Column({ type: 'varchar', length: 200 })
  name: string

  @OneToMany(() => Task, task => task.subject)
  tasks: Task[]

  @ManyToOne(() => Teacher, teacher => teacher.subjects)
  teacher: Teacher

  @ManyToOne(() => School, school => school.subjects)
  school: School
}
