import { Entity, PrimaryColumn, ManyToOne } from 'typeorm'
import { Student } from './student'
import { School } from './school'

@Entity({ name: 'subscription' })
export class Subscription {

  @PrimaryColumn({ type: 'varchar', length: 50 })
  id: string

  @ManyToOne(() => Student, student => student.subscriptions)
  student: Student

  @ManyToOne(() => School, school => school.subscriptions)
  school: School

}