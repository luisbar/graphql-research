import { Entity, PrimaryColumn, Column, ManyToOne } from 'typeorm'
import { Subject } from './subject'

@Entity({ name: 'task' })
export class Task {

  @PrimaryColumn({ type: 'varchar', length: 50 })
  id: string

  @Column({ type: 'varchar', length: 200 })
  name: string

  @Column({ type: 'varchar', length: 200 })
  description: string

  @Column({ type: 'varchar', length: 10 })
  dueDate: string

  @ManyToOne(() => Subject, subject => subject.tasks)
  subject: Subject

}