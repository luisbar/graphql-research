import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm'
import { Subject } from './subject'

@Entity({ name: 'teacher' })
export class Teacher {

  @PrimaryColumn({ type: 'varchar', length: 50 })
  id: string

  @Column({ type: 'varchar', length: 200 })
  fullName: string

  @Column({ type: 'varchar', length: 200 })
  email: string

  @Column({ type: 'int' })
  age: number

  @OneToMany(() => Subject, subject => subject.teacher)
  subjects: Subject[]

}