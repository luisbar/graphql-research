import { createConnection } from 'typeorm'
import { Teacher } from './entities/teacher'
import { Student } from './entities/student'
import { Task } from './entities/task'
import { Subject } from './entities/subject'
import { School } from './entities/school'
import { Subscription } from './entities/subscription'

export default createConnection({
  type: 'mysql',
  host: 'mysql',
  port: parseInt(process.env.DATBASE_PORT),
  username: process.env.DATBASE_USERNAME,
  password: process.env.DATBASE_PASSWORD,
  database: process.env.DATBASE_DATABASE,
  synchronize: process.env.DATBASE_SYNCHRONIZE ? true : false,
  migrationsRun: process.env.DATBASE_SYNCHRONIZE ? true : false,
  logging: true,
  entities: [ Teacher, Student, Task, Subject, School, Subscription ],
  migrations: [`${__dirname}/migrations/*.js`],
  cli: {
    migrationsDir: `${__dirname}/migrations`
  }
})