import { MigrationInterface, QueryRunner } from 'typeorm'
import memory from '../../memory'
const logger = console.log

export class SeedData1603413098904 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    try {
      await queryRunner
      .connection
      .getRepository('teacher')
      .save(memory.teachers)
      await queryRunner
      .connection
      .getRepository('student')
      .save(memory.students)
      await queryRunner
      .connection
      .getRepository('task')
      .save(memory.tasks)
      await queryRunner
      .connection
      .getRepository('subject')
      .save(memory.subjects)
      await queryRunner
      .connection
      .getRepository('school')
      .save(memory.schools)
      await queryRunner
      .connection
      .getRepository('subscription')
      .save(memory.subscriptions)
    } catch (error) {
      logger('Duplicated data')
    }
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
