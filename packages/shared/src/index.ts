import { datasources } from './datasources'
const schema = require('./schema')

export = {
  datasources,
  schema,
}