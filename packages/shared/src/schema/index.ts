import Teacher from './teacher'
import Student from './student'
import Task from './task'
import Subject from './subject'
import School from './school'
import Subscription from './subscription'

const Query = `
  type Query {
    teachers: [Teacher]
    students: [Student]
    tasks: [Task]
    subjects: [Subject]
    schools: [School]
    subscriptions: [Subscription]
    teacher(id: ID): Teacher
    student(id: ID): Student
    task(id: ID): Task
    subject(id: ID): Subject
    school(id: ID): School
    subscription(id: ID): Subscription
  }
`

export = [ Query, Teacher, Student, Task, Subject, School, Subscription ]