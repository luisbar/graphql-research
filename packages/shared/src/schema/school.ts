export default `
  type School {
    id: ID
    name: String
    subjects: [Subject]
    subscriptions: [Subscription]
  }
`