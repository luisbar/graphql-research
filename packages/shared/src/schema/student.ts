export default `
  type Student {
    id: ID
    fullName: String
    email: String
    age: Int
    subscriptions: [Subscription]
  }
`