export default `
  type Subject {
    id: ID
    name: String
    tasks: [Task]
    teacher: Teacher
    school: School
  }
`