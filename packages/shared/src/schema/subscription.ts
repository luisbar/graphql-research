export default `
  type Subscription {
    id: ID
    student: Student
    school: School
  }
`