export default `
  type Task {
    id: ID
    name: String
    description: String
    dueDate: String
    subject: Subject
  }
`