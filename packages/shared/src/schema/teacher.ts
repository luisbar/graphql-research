export default `
  type Teacher {
    id: ID
    fullName: String
    email: String
    age: Int
    subjects: [Subject]
}
`
